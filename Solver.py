from enum import Enum


class Correctness(Enum):
    NULL = 0
    RIGHT = 1
    WRONG = 2


class Position(Enum):
    NULL = 0
    RIGHT = 1
    WRONG = 2


class Rule:

    def __init__(self, sequence, count, correctness, position):
        self.sequence = sequence
        self.count = count
        self.correctness = correctness
        self.position = position


# Wrong Keys
wrong_keys = list()

# Rules
key_rules = list()


def add_rule(sequence, count, correctness, position):
    # We know all of the values
    if len(sequence) == count:
        if correctness == Correctness.WRONG:
            for key in sequence:
                wrong_keys.append(key)

    # Add the rules
    else:
        key_rules.append(Rule(sequence, count, correctness, position))


def swap(array, a, b):
    temp = array[a]
    array[a] = array[b]
    array[b] = temp


def test_rule(solution, rule):
    # Check if contains wrong element
    for key in wrong_keys:
        if key in solution:
            return False

    # Check rule
    right_correctness_count = 0
    wrong_correctness_count = 0
    right_position_count = 0
    wrong_position_count = 0

    for i in range(0, len(solution)):
        # Check for correctness
        if solution[i] in rule.sequence:
            right_correctness_count += 1
            # Check for position
            if solution[i] == rule.sequence[i]:
                right_position_count += 1
            else:
                wrong_position_count += 1
        else:
            wrong_correctness_count += 1

    if rule.correctness == Correctness.RIGHT:
        if not right_correctness_count == rule.count:
            return False
    elif rule.correctness == Correctness.WRONG:
        if not wrong_correctness_count == rule.count:
            return False

    if rule.position == Position.RIGHT:
        if not right_position_count == rule.count:
            return False
    elif rule.position == Position.WRONG:
        if not wrong_position_count == rule.count:
            return False

    return True


def test_solution(solution):
    for rule in key_rules:
        if not test_rule(solution, rule):
            return False
    return True


def solve():
    # Solve for correct keys
    solutions = list()

    temp_elements = list()
    for rule in key_rules:
        for element in rule.sequence:
            if element not in temp_elements:
                temp_elements.append(element)

    # Iterative heap's algorithm for generating permutations
    n = len(temp_elements)

    c = list()
    for i in range(0, n):
        c.append(0)

    i = 0
    while i < n:
        if c[i] < i:
            if i % 2 == 0:
                swap(temp_elements, 0, i)
            else:
                swap(temp_elements, c[i], i)

            solution = list()
            solution.append(temp_elements[0])
            solution.append(temp_elements[1])
            solution.append(temp_elements[2])

            if test_solution(solution):
                if solution not in solutions:
                    solutions.append(solution)

            c[i] += 1
            i = 0
        else:
            c[i] = 0
            i += 1

    temp_elements.clear()

    return solutions


if __name__ == "__main__":
    add_rule([6, 8, 2], 1, Correctness.RIGHT, Position.RIGHT)
    add_rule([6, 1, 4], 1, Correctness.RIGHT, Position.WRONG)
    add_rule([2, 0, 6], 2, Correctness.RIGHT, Position.WRONG)
    add_rule([7, 3, 8], 3, Correctness.WRONG, Position.NULL)
    add_rule([3, 8, 0], 1, Correctness.RIGHT, Position.WRONG)
    answers = solve()
    for answer in answers:
        print(answer)