# Lock Puzzle

This is a simple problem to solve the Lock Puzzle described in the problem below.
I modeled the problem as a series of rules and solved by trying all of the permutations against the rule constraints.
I used Heap's algorithm for generating the permutations, 
and my rules were basic evaluations of whether the counts of matches from the rules were equivalent. 
The program will output a list of solutions if there are multiple, a single solution if there is one, 
or none if there isn't a solution.

# The Problem
Can you open the lock using these clues?
- 682, One digit is right and in it's place
- 614, One digit is right but in the wrong place
- 206, Two digits are right but both are in the wrong place
- 738, All digits are wrong
- 380, One digit is right but in the wrong place


## Getting Started

To use the program simply run python Solver.py. Ensure that you have python in your path if python is unfound.
On some operating systems you might have to use python3. I have not tested, but this program should be crossplatform.

### Prerequisites

`>=Python3.4 (due to use of Enum)


## Authors

* **Brendon Lantz**


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
